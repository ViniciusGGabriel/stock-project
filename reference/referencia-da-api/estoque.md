---
description: Crie um novo computador no estoque
---

# Estoque

<mark style="color:green;">`POST`</mark> `https://api.myapi.com/v2/estoque/computers`

Body

201 Computador criado com sucesso
```json
{
  "dadosGerais": {
    "condicoes": "USADO",
    "numeroPatrimonio": "string",
    "localPc": "MATRIZ",
    "marcaPc": "string"
  },
  "hardware": {
    "tipoComputador": "DESKTOP",
    "nomeComputador": "string",
    "ip": "string",
    "cpu": "string",
    "memoriaRamGb": 0,
    "frequenciaRam": 0,
    "tipoRam": "DIMM",
    "modeloRam": "DDR4",
    "quantRamInstalada": 0,
    "hdGb": 0,
    "ssdGb": 0
  },
  "software": {
    "tasyAgent": true,
    "so": "W11"
  }
}
```
