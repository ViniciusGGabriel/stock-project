---
description: >-
  Esse projeto se consiste em um sistema de gerenciamento de computadores
  presentes em estoque, matriz e postos
---

# Seja bem-vindo ao meu projeto

## Iniciando pelo back-end

Meu back-end se consiste em uma API produzida em java + Spring Boot e o banco de dados PostgreSQL juntamente ao Docker para levantar todas dependências

## Quer ver?

Acesse o link a baixo e faça sua primeira requisição

[comecando-de-forma-rapida.md](comecando-de-forma-rapida.md)

## Quer verificar antes de fazer uma requisição

Clique no link a baixo para ver a referência das requisições para a API

[referencia-da-api](reference/referencia-da-api/)


## Front-end

O front-end foi inteiramente feito em Next.js + Material IU para criação de uma interface mais prática e bonita
