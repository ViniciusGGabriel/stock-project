package com.stock.backend.dto.matriz;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.stock.backend.dto.base.Hardware;
import com.stock.backend.dto.base.Software;
import com.stock.backend.model.entities.ComputerMatrizEntity;
import lombok.Builder;

import java.util.UUID;

@Builder
public record ComputerMatrizResponseDto(
        @JsonProperty("identificador") UUID idPc,
        @JsonProperty("dadosGerais") DadosGeraisMatriz dadosGerais,
        @JsonProperty("hardware") Hardware hardware,
        @JsonProperty("software") Software software,
        @JsonProperty("links") Links links
) {

    public ComputerMatrizResponseDto(ComputerMatrizEntity matrizEntity) {
        this(matrizEntity.getIdPc(),
                new DadosGeraisMatriz(matrizEntity.getDepartamento(), matrizEntity.getUnidadeDeNegocio(),
                        matrizEntity.getNumeroPatrimonio(), matrizEntity.getLocalPc(),
                        matrizEntity.getMarcaPc()),

                new Hardware(matrizEntity.getTipoComputador(), matrizEntity.getNomeComputador(),
                        matrizEntity.getIpv4(), matrizEntity.getCpu(),
                        matrizEntity.getMemoriaRamGb(), matrizEntity.getFrequenciaRam(),
                        matrizEntity.getTipoRam(), matrizEntity.getModeloRam(),
                        matrizEntity.getQuantRamInstalada(), matrizEntity.getHdGb(),
                        matrizEntity.getSsdGb()),

                new Software(matrizEntity.isTasyAgent(), matrizEntity.getSo()),

                new Links(matrizEntity.getIdPc()));
    }
}

record Links(
        @JsonProperty("getById") String getByIdLink
) {
    public Links(UUID getByIdLink) {
        this("http://localhost:8080/v1/matriz/computers/" + getByIdLink);
    }
}