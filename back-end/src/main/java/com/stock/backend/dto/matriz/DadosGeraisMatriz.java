package com.stock.backend.dto.matriz;

import com.stock.backend.model.typesEnum.TypeBusinessUnit;
import com.stock.backend.model.typesEnum.TypeLocalComputer;

public record DadosGeraisMatriz(String departamento, TypeBusinessUnit unidadeDeNegocio, String numeroPatrimonio,
                                TypeLocalComputer localPc, String marcaPc
) {
}