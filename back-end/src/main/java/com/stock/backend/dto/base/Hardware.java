package com.stock.backend.dto.base;

import com.stock.backend.model.typesEnum.TypeComputer;
import com.stock.backend.model.typesEnum.TypeModelRam;
import com.stock.backend.model.typesEnum.TypeRam;

public record Hardware(
        TypeComputer tipoComputador, String nomeComputador,
        String ip, String cpu, Integer memoriaRamGb,
        Integer frequenciaRam, TypeRam tipoRam, TypeModelRam modeloRam,
        Integer quantRamInstalada, Integer hdGb, Integer ssdGb
) {
}
