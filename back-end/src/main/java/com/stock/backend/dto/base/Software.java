package com.stock.backend.dto.base;

import com.stock.backend.model.typesEnum.TypeSo;

public record Software(
        boolean tasyAgent, TypeSo so
) {
}
