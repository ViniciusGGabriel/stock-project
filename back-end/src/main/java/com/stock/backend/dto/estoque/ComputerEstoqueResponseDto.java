package com.stock.backend.dto.estoque;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.stock.backend.dto.base.Hardware;
import com.stock.backend.dto.base.Software;
import com.stock.backend.model.entities.ComputerEstoqueEntity;
import lombok.Builder;

import java.util.UUID;

@Builder
public record ComputerEstoqueResponseDto(
        @JsonProperty("identificador") UUID idPc,
        @JsonProperty("dadosGerais") DadosGeraisEstoque dadosGeraisEstoque,
        @JsonProperty("hardware") Hardware hardware,
        @JsonProperty("software") Software software,
        @JsonProperty("links") Links links
) {

    public ComputerEstoqueResponseDto(ComputerEstoqueEntity estoqueEntity) {
        this(estoqueEntity.getIdPc(),
                new DadosGeraisEstoque(estoqueEntity.getCondicoes(), estoqueEntity.getNumeroPatrimonio(),
                        estoqueEntity.getLocalPc(), estoqueEntity.getMarcaPc()),

                new Hardware(estoqueEntity.getTipoComputador(), estoqueEntity.getNomeComputador(),
                        estoqueEntity.getIpv4(), estoqueEntity.getCpu(),
                        estoqueEntity.getMemoriaRamGb(), estoqueEntity.getFrequenciaRam(),
                        estoqueEntity.getTipoRam(), estoqueEntity.getModeloRam(),
                        estoqueEntity.getQuantRamInstalada(), estoqueEntity.getHdGb(),
                        estoqueEntity.getSsdGb()),

                new Software(estoqueEntity.isTasyAgent(), estoqueEntity.getSo()),

                new Links(estoqueEntity.getIdPc()));
    }
}

record Links(
        @JsonProperty("getById") String getByIdLink
) {
    public Links(UUID getByIdLink) {
        this("http://localhost:8080/v1/estoque/computers/" + getByIdLink);
    }
}