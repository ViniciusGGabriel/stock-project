package com.stock.backend.dto.posto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.stock.backend.dto.base.Hardware;
import com.stock.backend.dto.base.Software;
import com.stock.backend.model.entities.ComputerPostoEntity;
import lombok.Builder;

import java.util.UUID;

@Builder
public record ComputerPostoResponseDto(
        @JsonProperty("identificador") UUID idPc,
        @JsonProperty("dadosGerais") DadosGeraisPosto dadosGeraisPosto,
        @JsonProperty("hardware") Hardware hardware,
        @JsonProperty("software") Software software,
        @JsonProperty("links") Links links
) {

    public ComputerPostoResponseDto(ComputerPostoEntity postoEntity) {
        this(postoEntity.getIdPc(),
                new DadosGeraisPosto(postoEntity.getDepartamento(), postoEntity.getLocalPosto(),
                        postoEntity.getNumeroPatrimonio(), postoEntity.getLocalPc(), postoEntity.getMarcaPc()),

                new Hardware(postoEntity.getTipoComputador(), postoEntity.getNomeComputador(),
                        postoEntity.getIpv4(), postoEntity.getCpu(),
                        postoEntity.getMemoriaRamGb(), postoEntity.getFrequenciaRam(),
                        postoEntity.getTipoRam(), postoEntity.getModeloRam(),
                        postoEntity.getQuantRamInstalada(), postoEntity.getHdGb(),
                        postoEntity.getSsdGb()),

                new Software(postoEntity.isTasyAgent(), postoEntity.getSo()),

                new Links(postoEntity.getIdPc()));
    }
}

record Links(
        @JsonProperty("getById") String getByIdLink
) {
    public Links(UUID getByIdLink) {
        this("http://localhost:8080/v1/posto/computers/" + getByIdLink);
    }
}
