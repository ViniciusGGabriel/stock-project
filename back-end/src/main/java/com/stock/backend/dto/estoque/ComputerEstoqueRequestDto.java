package com.stock.backend.dto.estoque;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.stock.backend.dto.base.Hardware;
import com.stock.backend.dto.base.Software;
import lombok.Builder;

@Builder
public record ComputerEstoqueRequestDto(@JsonProperty("dadosGerais") DadosGeraisEstoque dadosGeraisEstoque,
                                        @JsonProperty("hardware") Hardware hardware,
                                        @JsonProperty("software") Software software) {
}
