package com.stock.backend.dto.matriz;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.stock.backend.dto.base.Hardware;
import com.stock.backend.dto.base.Software;
import lombok.Builder;

@Builder
public record ComputerMatrizRequestDto(@JsonProperty("dadosGerais") DadosGeraisMatriz dadosGeraisMatriz,
                                       @JsonProperty("hardware") Hardware hardware,
                                       @JsonProperty("software") Software software) {

}
