package com.stock.backend.dto.posto;

import com.stock.backend.model.typesEnum.TypeDepartmentStation;
import com.stock.backend.model.typesEnum.TypeLocalComputer;
import com.stock.backend.model.typesEnum.TypeLocalStation;

public record DadosGeraisPosto(TypeDepartmentStation departamento, TypeLocalStation localPosto, String numeroPatrimonio,
                               TypeLocalComputer localPc, String marcaPc
) {
}

