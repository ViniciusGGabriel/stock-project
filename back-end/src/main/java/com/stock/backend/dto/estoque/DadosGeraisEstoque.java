package com.stock.backend.dto.estoque;

import com.stock.backend.model.typesEnum.TypeCondicoes;
import com.stock.backend.model.typesEnum.TypeLocalComputer;

public record DadosGeraisEstoque(TypeCondicoes condicoes, String numeroPatrimonio, TypeLocalComputer localPc,
                                 String marcaPc
) {
}