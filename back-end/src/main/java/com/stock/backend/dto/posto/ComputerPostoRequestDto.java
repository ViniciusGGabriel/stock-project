package com.stock.backend.dto.posto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.stock.backend.dto.base.Hardware;
import com.stock.backend.dto.base.Software;
import lombok.Builder;

@Builder
public record ComputerPostoRequestDto(@JsonProperty("dadosGerais") DadosGeraisPosto dadosGeraisPosto,
                                      @JsonProperty("hardware") Hardware hardware,
                                      @JsonProperty("software") Software software) {
}

