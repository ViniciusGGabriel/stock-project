package com.stock.backend.repositories;

import com.stock.backend.model.entities.ComputerEstoqueEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface ComputerEstoqueRepoFactory extends JpaRepository<ComputerEstoqueEntity, UUID> {
}
