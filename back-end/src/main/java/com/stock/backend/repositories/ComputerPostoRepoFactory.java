package com.stock.backend.repositories;

import com.stock.backend.model.entities.ComputerPostoEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface ComputerPostoRepoFactory extends JpaRepository<ComputerPostoEntity, UUID> {
}
