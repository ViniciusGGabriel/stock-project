package com.stock.backend.repositories;

import com.stock.backend.model.entities.ComputerMatrizEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface ComputerMatrizRepoFactory extends JpaRepository<ComputerMatrizEntity, UUID> {
}
