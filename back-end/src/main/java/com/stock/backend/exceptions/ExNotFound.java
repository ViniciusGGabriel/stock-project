package com.stock.backend.exceptions;

public class ExNotFound extends RuntimeException {
    public ExNotFound(String meString) {
        super(meString);
    }
}
