package com.stock.backend.exceptions.exceptionsHandler.serviceHandler;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

@AllArgsConstructor
@Getter
@Setter
public class DaoOperationStatus {
    private HttpStatus httpStatus;
    public String httpMessage;
}
