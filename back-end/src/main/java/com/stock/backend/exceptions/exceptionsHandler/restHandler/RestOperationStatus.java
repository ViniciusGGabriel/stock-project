package com.stock.backend.exceptions.exceptionsHandler.restHandler;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

@AllArgsConstructor
@Getter
@Setter
public class RestOperationStatus {
    private HttpStatus httpStatus;
    public String httpMessage;
}
