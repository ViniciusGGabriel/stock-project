package com.stock.backend.exceptions.exceptionsHandler.serviceHandler;

import com.stock.backend.exceptions.ExNotFound;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice // NOTE: Quando o controller for executado passara por esse tratamento de erros
public class DaoExceptionHandler {
    private DaoOperationStatus daoOperationStatus;

    // NOTE: Internal server error
    private ResponseEntity<DaoOperationStatus> internalServerError() {
        daoOperationStatus = new DaoOperationStatus(HttpStatus.INTERNAL_SERVER_ERROR, "Ocorreu um erro interno no servidor.");
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(daoOperationStatus);
    }

    @ExceptionHandler(ExNotFound.class)
    private ResponseEntity<DaoOperationStatus> computerUUIdNotFound(ExNotFound exception) {
        try {
            daoOperationStatus = new DaoOperationStatus(HttpStatus.NOT_FOUND, exception.getMessage());
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(daoOperationStatus);
        } catch (Exception exceptionInternal) {
            return internalServerError();
        }
    }
}
