package com.stock.backend.exceptions.exceptionsHandler.restHandler;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice // NOTE: Quando o controller for executado passara por esse tratamento de erros
public class RestExceptionHandler extends ResponseEntityExceptionHandler {
    private RestOperationStatus restOperationStatus;

    // NOTE: Internal server error
    private ResponseEntity<RestOperationStatus> internalServerError() {
        restOperationStatus = new RestOperationStatus(HttpStatus.INTERNAL_SERVER_ERROR, "Ocorreu um erro interno no servidor.");
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(restOperationStatus);
    }

    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    private ResponseEntity<RestOperationStatus> computerUUIInvalidArgument(MethodArgumentTypeMismatchException exception) {
        try {
            restOperationStatus = new RestOperationStatus(HttpStatus.BAD_REQUEST, "UUID inserido é invalido");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(restOperationStatus);
        } catch (Exception exceptionInternal) {
            return internalServerError();
        }
    }

    @ExceptionHandler(DataIntegrityViolationException.class)
    private ResponseEntity<RestOperationStatus> computerDataViolationHandler(DataIntegrityViolationException exception) {
        try {
            if (exception.getMessage().contains("duplicate key value violates unique constraint")) {
                restOperationStatus = new RestOperationStatus(HttpStatus.CONFLICT, "Valor inserido já é existente dentro do banco!");
            } else {
                restOperationStatus = new RestOperationStatus(HttpStatus.BAD_REQUEST, "Dados inseridos estão errados!");
            }

            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(restOperationStatus);
        } catch (Exception exceptionInternal) {
            return internalServerError();
        }
    }
}