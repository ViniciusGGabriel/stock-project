package com.stock.backend.model.typesEnum;

public enum TypeDepartmentStation {
    AREA_TECNICA,
    PRONTO_SOCORRO,
    RECEPCAO,
    COLETA
}
