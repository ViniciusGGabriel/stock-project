package com.stock.backend.model.typesEnum;

public enum TypeBusinessUnit {
    HOSPITAL,
    LABORATORIO,
    BANCO_DE_SANGUE,
    CEMEG
}
