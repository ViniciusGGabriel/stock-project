package com.stock.backend.model.entities;

import com.stock.backend.dto.estoque.ComputerEstoqueRequestDto;
import com.stock.backend.model.BaseComputerEntity;
import com.stock.backend.model.typesEnum.TypeCondicoes;
import jakarta.persistence.*;
import lombok.*;

@Entity
@Table(name = "computadores_estoque")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class ComputerEstoqueEntity extends BaseComputerEntity {
    @Column(nullable = false, length = 16)
    @Enumerated(EnumType.STRING)
    private TypeCondicoes condicoes;

    public ComputerEstoqueEntity(ComputerEstoqueRequestDto computerEstoqueRequestDto) {
        this.condicoes = computerEstoqueRequestDto.dadosGeraisEstoque().condicoes();
        this.setNumeroPatrimonio(computerEstoqueRequestDto.dadosGeraisEstoque().numeroPatrimonio());
        this.setTipoComputador(computerEstoqueRequestDto.hardware().tipoComputador());
        this.setNomeComputador(computerEstoqueRequestDto.hardware().nomeComputador());
        this.setIpv4(computerEstoqueRequestDto.hardware().ip());
        this.setCpu(computerEstoqueRequestDto.hardware().cpu());
        this.setMemoriaRamGb(computerEstoqueRequestDto.hardware().memoriaRamGb());
        this.setFrequenciaRam(computerEstoqueRequestDto.hardware().frequenciaRam());
        this.setTipoRam(computerEstoqueRequestDto.hardware().tipoRam());
        this.setModeloRam(computerEstoqueRequestDto.hardware().modeloRam());
        this.setQuantRamInstalada(computerEstoqueRequestDto.hardware().quantRamInstalada());
        this.setHdGb(computerEstoqueRequestDto.hardware().hdGb());
        this.setSsdGb(computerEstoqueRequestDto.hardware().ssdGb());
        this.setTasyAgent(computerEstoqueRequestDto.software().tasyAgent());
        this.setLocalPc(computerEstoqueRequestDto.dadosGeraisEstoque().localPc());
        this.setMarcaPc(computerEstoqueRequestDto.dadosGeraisEstoque().marcaPc());
        this.setSo(computerEstoqueRequestDto.software().so());
    }
}
