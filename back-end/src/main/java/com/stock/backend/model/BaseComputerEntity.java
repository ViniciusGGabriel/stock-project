package com.stock.backend.model;

import com.stock.backend.model.typesEnum.*;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@MappedSuperclass
public class BaseComputerEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID idPc;

    @Column(length = 6)
    private String numeroPatrimonio;

    @Column(length = 15)
    @Enumerated(EnumType.STRING)
    private TypeComputer tipoComputador;

    @Column(nullable = false, length = 15)
    private String nomeComputador;

    @Column(nullable = false, length = 15, unique = true)
    private String ipv4;

    @Column(length = 15)
    private String cpu;

    private Integer memoriaRamGb;
    private Integer frequenciaRam;

    @Column(length = 10)
    @Enumerated(EnumType.STRING)
    private TypeRam tipoRam;

    @Column(length = 5)
    @Enumerated(EnumType.STRING)
    private TypeModelRam modeloRam;

    private Integer quantRamInstalada;
    private Integer hdGb;
    private Integer ssdGb;
    private boolean tasyAgent;

    @Column(nullable = false, length = 15)
    @Enumerated(EnumType.STRING)
    private TypeLocalComputer localPc;

    @Column(length = 15)
    private String marcaPc;

    @Column(length = 3)
    @Enumerated(EnumType.STRING)
    private TypeSo so;

    public BaseComputerEntity(UUID idPc) {
        this.idPc = idPc;
    }
}
