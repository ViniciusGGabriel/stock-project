package com.stock.backend.model.entities;

import com.stock.backend.dto.posto.ComputerPostoRequestDto;
import com.stock.backend.model.BaseComputerEntity;
import com.stock.backend.model.typesEnum.TypeDepartmentStation;
import com.stock.backend.model.typesEnum.TypeLocalStation;
import jakarta.persistence.*;
import lombok.*;

@Entity
@Table(name = "computadores_postos")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class ComputerPostoEntity extends BaseComputerEntity {
    @Column(nullable = false, length = 16)
    @Enumerated(EnumType.STRING)
    private TypeDepartmentStation departamento;

    @Column(nullable = false, length = 22)
    @Enumerated(EnumType.STRING)
    private TypeLocalStation localPosto;


    // NOTE: Constructor para criação do DTO
    public ComputerPostoEntity(ComputerPostoRequestDto requestDto) {
        this.departamento = requestDto.dadosGeraisPosto().departamento();
        this.localPosto = requestDto.dadosGeraisPosto().localPosto();
        this.setNumeroPatrimonio(requestDto.dadosGeraisPosto().numeroPatrimonio());
        this.setTipoComputador(requestDto.hardware().tipoComputador());
        this.setNomeComputador(requestDto.hardware().nomeComputador());
        this.setIpv4(requestDto.hardware().ip());
        this.setCpu(requestDto.hardware().cpu());
        this.setMemoriaRamGb(requestDto.hardware().memoriaRamGb());
        this.setFrequenciaRam(requestDto.hardware().frequenciaRam());
        this.setTipoRam(requestDto.hardware().tipoRam());
        this.setModeloRam(requestDto.hardware().modeloRam());
        this.setQuantRamInstalada(requestDto.hardware().quantRamInstalada());
        this.setHdGb(requestDto.hardware().hdGb());
        this.setSsdGb(requestDto.hardware().ssdGb());
        this.setTasyAgent(requestDto.software().tasyAgent());
        this.setSo(requestDto.software().so());
    }
}
