package com.stock.backend.model.typesEnum;

public enum TypeComputer {
    DESKTOP,
    MONTADO,
    ALL_IN_ONE,
    MICRO,
    NOTEBOOK
}
