package com.stock.backend.model.typesEnum;

public enum TypeModelRam {
    DDR4,
    DRR3
}
