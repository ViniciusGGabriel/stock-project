package com.stock.backend.model.entities;

import com.stock.backend.dto.matriz.ComputerMatrizRequestDto;
import com.stock.backend.model.BaseComputerEntity;
import com.stock.backend.model.typesEnum.TypeBusinessUnit;
import jakarta.persistence.*;
import lombok.*;

@Entity
@Table(name = "computadores_matriz")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class ComputerMatrizEntity extends BaseComputerEntity {
    @Column(nullable = false, length = 100)
    private String departamento;

    @Column(nullable = false, length = 16)
    @Enumerated(EnumType.STRING)
    private TypeBusinessUnit unidadeDeNegocio;

    // NOTE: Constructor para criação do DTO
    public ComputerMatrizEntity(ComputerMatrizRequestDto computerMatrizRequestDto) {
        this.departamento = computerMatrizRequestDto.dadosGeraisMatriz().departamento();
        this.unidadeDeNegocio = computerMatrizRequestDto.dadosGeraisMatriz().unidadeDeNegocio();
        this.setNumeroPatrimonio(computerMatrizRequestDto.dadosGeraisMatriz().numeroPatrimonio());
        this.setTipoComputador(computerMatrizRequestDto.hardware().tipoComputador());
        this.setNomeComputador(computerMatrizRequestDto.hardware().nomeComputador());
        this.setIpv4(computerMatrizRequestDto.hardware().ip());
        this.setCpu(computerMatrizRequestDto.hardware().cpu());
        this.setMemoriaRamGb(computerMatrizRequestDto.hardware().memoriaRamGb());
        this.setFrequenciaRam(computerMatrizRequestDto.hardware().frequenciaRam());
        this.setTipoRam(computerMatrizRequestDto.hardware().tipoRam());
        this.setModeloRam(computerMatrizRequestDto.hardware().modeloRam());
        this.setQuantRamInstalada(computerMatrizRequestDto.hardware().quantRamInstalada());
        this.setHdGb(computerMatrizRequestDto.hardware().hdGb());
        this.setSsdGb(computerMatrizRequestDto.hardware().ssdGb());
        this.setTasyAgent(computerMatrizRequestDto.software().tasyAgent());
        this.setLocalPc(computerMatrizRequestDto.dadosGeraisMatriz().localPc());
        this.setMarcaPc(computerMatrizRequestDto.dadosGeraisMatriz().marcaPc());
        this.setSo(computerMatrizRequestDto.software().so());
    }
}