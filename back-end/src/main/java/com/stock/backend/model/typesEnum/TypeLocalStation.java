package com.stock.backend.model.typesEnum;

public enum TypeLocalStation {
    IRG,
    SANTA_BARBARA,
    HOSPITAL_CORACAO,
    SAO_FRANCISCO,
    ORION,
    HIC
}
