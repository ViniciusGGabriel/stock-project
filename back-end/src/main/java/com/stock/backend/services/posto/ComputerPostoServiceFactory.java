package com.stock.backend.services.posto;

import com.stock.backend.dto.posto.ComputerPostoRequestDto;
import com.stock.backend.dto.posto.ComputerPostoResponseDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.UUID;

public interface ComputerPostoServiceFactory {
    Page<ComputerPostoResponseDto> getAllComputersInPosto(Pageable pageable);

    ComputerPostoResponseDto getComputerInPostoByUUID(UUID uuid);

    void setComputerInPosto(ComputerPostoRequestDto computerInPosto);

    void putComputerInPosto(UUID uuid, ComputerPostoRequestDto computerPutInPosto);

    void patchComputerInPosto(UUID uuid, ComputerPostoRequestDto computerPatchInPosto);

    void deleteComputerInPosto(UUID uuid);
}
