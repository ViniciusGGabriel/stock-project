package com.stock.backend.services.matriz;

import com.stock.backend.dto.matriz.ComputerMatrizRequestDto;
import com.stock.backend.dto.matriz.ComputerMatrizResponseDto;
import com.stock.backend.exceptions.ExNotFound;
import com.stock.backend.model.entities.ComputerMatrizEntity;
import com.stock.backend.repositories.ComputerMatrizRepoFactory;
import com.stock.backend.ultils.GetNullPropertyNames;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class ComputerMatrizService implements ComputerMatrizServiceFactory {
    @Autowired
    private final ComputerMatrizRepoFactory computerMatrizRepoFactory;
    private ComputerMatrizEntity computerMatrizEntity;

    public ComputerMatrizService(ComputerMatrizRepoFactory computerMatrizRepoFactory) {
        this.computerMatrizRepoFactory = computerMatrizRepoFactory;
    }

    @Override
    public Page<ComputerMatrizResponseDto> getAllComputersInMatriz(Pageable pageable) {
        return computerMatrizRepoFactory.findAll(pageable).map(ComputerMatrizResponseDto::new);
    }

    @Override
    public ComputerMatrizResponseDto getComputerInMatrizByUUID(UUID uuid) {
        // NOTE: os catch salvam erros para ser verificados no RestException
        try {
            computerMatrizEntity = computerMatrizRepoFactory.findById(uuid).orElseThrow(() -> new ExNotFound("Computador com uuid não encontrado: " + uuid));
            return new ComputerMatrizResponseDto(computerMatrizEntity);
        } catch (EntityNotFoundException exception) {
            throw new ExNotFound("Computador com uuid não encontrado: " + uuid);
        }
    }

    @Override
    public void setComputerInMatriz(ComputerMatrizRequestDto computerSetInMatriz) {
        computerMatrizEntity = new ComputerMatrizEntity(computerSetInMatriz);
        computerMatrizRepoFactory.save(computerMatrizEntity);
    }

    // NOTE: O que não for preenchido dentro do banco de dados será NULL
    @Override
    public void putComputerInMatriz(UUID uuid, ComputerMatrizRequestDto computerPutInMatriz) {
        computerMatrizEntity = computerMatrizRepoFactory.findById(uuid).orElseThrow(() -> new ExNotFound("Computador com uuid não encontrado: " + uuid));

        // NOTE: (novoValor, valorAtual, valorIgnorado)
        BeanUtils.copyProperties(computerPutInMatriz, computerMatrizEntity, "idPc");
        computerMatrizRepoFactory.save(computerMatrizEntity);
    }

    @Override
    public void patchComputerInMatriz(UUID uuid, ComputerMatrizRequestDto computerPatchInMatriz) {
        computerMatrizEntity = computerMatrizRepoFactory.findById(uuid).orElseThrow(() -> new ExNotFound("Computador com uuid não encontrado: " + uuid));

        String[] getNullPropertyNames = GetNullPropertyNames.nullPropertyNames(computerPatchInMatriz);

        // NOTE: (novoValor, valorAtual, valorIgnorado)
        BeanUtils.copyProperties(computerPatchInMatriz, computerMatrizEntity, getNullPropertyNames);
        computerMatrizRepoFactory.save(computerMatrizEntity);
    }

    @Override
    public void deleteComputerInMatriz(UUID uuid) {
        computerMatrizEntity = computerMatrizRepoFactory.findById(uuid).orElseThrow(() -> new ExNotFound("Computador com uuid não encontrado: " + uuid));
        computerMatrizRepoFactory.delete(computerMatrizEntity);
    }
}
