package com.stock.backend.services.estoque;

import com.stock.backend.dto.estoque.ComputerEstoqueRequestDto;
import com.stock.backend.dto.estoque.ComputerEstoqueResponseDto;
import com.stock.backend.exceptions.ExNotFound;
import com.stock.backend.model.entities.ComputerEstoqueEntity;
import com.stock.backend.repositories.ComputerEstoqueRepoFactory;
import com.stock.backend.ultils.GetNullPropertyNames;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class ComputerEstoqueService implements ComputerEstoqueServiceFactory {
    @Autowired
    private final ComputerEstoqueRepoFactory computerEstoqueRepoFactory;
    private ComputerEstoqueEntity computerEstoqueEntity;

    public ComputerEstoqueService(ComputerEstoqueRepoFactory computerEstoqueRepoFactory) {
        this.computerEstoqueRepoFactory = computerEstoqueRepoFactory;
    }

    @Override
    public Page<ComputerEstoqueResponseDto> getAllComputersInEstoque(Pageable pageable) {
        return computerEstoqueRepoFactory.findAll(pageable).map(ComputerEstoqueResponseDto::new);
    }

    @Override
    public ComputerEstoqueResponseDto getComputerInEstoqueByUUID(UUID uuid) {
        // NOTE: os catch salvam erros para ser verificados no RestException
        computerEstoqueEntity = computerEstoqueRepoFactory.findById(uuid).orElseThrow(() -> new ExNotFound("Computador com uuid não encontrado: " + uuid));
        return new ComputerEstoqueResponseDto(computerEstoqueEntity);
    }

    @Override
    public void setComputerInEstoque(ComputerEstoqueRequestDto computerInEstoque) {
        computerEstoqueEntity = new ComputerEstoqueEntity(computerInEstoque);
        computerEstoqueRepoFactory.save(computerEstoqueEntity);
    }

    @Override
    public void putComputerInEstoque(UUID uuid, ComputerEstoqueRequestDto computerPutInEstoque) {
        computerEstoqueEntity = computerEstoqueRepoFactory.findById(uuid).orElseThrow(() -> new ExNotFound("Computador com uuid não encontrado: " + uuid));

        // NOTE: (novoValor, valorAtual, valorIgnorado)
        BeanUtils.copyProperties(computerPutInEstoque, computerEstoqueEntity, "idPc");
        computerEstoqueRepoFactory.save(computerEstoqueEntity);
    }

    @Override
    public void patchComputerInEstoque(UUID uuid, ComputerEstoqueRequestDto computerPatchInEstoque) {
        computerEstoqueEntity = computerEstoqueRepoFactory.findById(uuid).orElseThrow(() -> new ExNotFound("Computador com uuid não encontrado: " + uuid));

        String[] getNullPropertyNames = GetNullPropertyNames.nullPropertyNames(computerPatchInEstoque);

        // NOTE: (novoValor, valorAtual, valorIgnorado)
        BeanUtils.copyProperties(computerPatchInEstoque, computerEstoqueEntity, getNullPropertyNames);
        computerEstoqueRepoFactory.save(computerEstoqueEntity);
    }

    @Override
    public void deleteComputerInEstoque(UUID uuid) {
        computerEstoqueEntity = computerEstoqueRepoFactory.findById(uuid).orElseThrow(() -> new ExNotFound("Computador com uuid não encontrado: " + uuid));
        computerEstoqueRepoFactory.delete(computerEstoqueEntity);
    }
}
