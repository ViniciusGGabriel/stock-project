package com.stock.backend.services.estoque;

import com.stock.backend.dto.estoque.ComputerEstoqueRequestDto;
import com.stock.backend.dto.estoque.ComputerEstoqueResponseDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.UUID;

public interface ComputerEstoqueServiceFactory {
    Page<ComputerEstoqueResponseDto> getAllComputersInEstoque(Pageable pageable);

    ComputerEstoqueResponseDto getComputerInEstoqueByUUID(UUID uuid);

    void setComputerInEstoque(ComputerEstoqueRequestDto computerInEstoque);

    void putComputerInEstoque(UUID uuid, ComputerEstoqueRequestDto computerPutInEstoque);

    void patchComputerInEstoque(UUID uuid, ComputerEstoqueRequestDto computerPatchInEstoque);

    void deleteComputerInEstoque(UUID uuid);
}
