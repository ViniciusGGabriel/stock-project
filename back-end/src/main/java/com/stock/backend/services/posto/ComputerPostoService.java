package com.stock.backend.services.posto;

import com.stock.backend.dto.posto.ComputerPostoRequestDto;
import com.stock.backend.dto.posto.ComputerPostoResponseDto;
import com.stock.backend.exceptions.ExNotFound;
import com.stock.backend.model.entities.ComputerPostoEntity;
import com.stock.backend.repositories.ComputerPostoRepoFactory;
import com.stock.backend.ultils.GetNullPropertyNames;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class ComputerPostoService implements ComputerPostoServiceFactory {
    @Autowired
    private final ComputerPostoRepoFactory computerPostoRepoFactory;
    private ComputerPostoEntity computerPostoEntity;

    public ComputerPostoService(ComputerPostoRepoFactory computerPostoRepoFactory) {
        this.computerPostoRepoFactory = computerPostoRepoFactory;
    }

    @Override
    public Page<ComputerPostoResponseDto> getAllComputersInPosto(Pageable pageable) {
        return computerPostoRepoFactory.findAll(pageable).map(ComputerPostoResponseDto::new);
    }

    @Override
    public ComputerPostoResponseDto getComputerInPostoByUUID(UUID uuid) {
        // NOTE: os catch salvam erros para ser verificados no RestException
        computerPostoEntity = computerPostoRepoFactory.findById(uuid).orElseThrow(() -> new ExNotFound("Computador com uuid não encontrado: " + uuid));
            return new ComputerPostoResponseDto(computerPostoEntity);
    }

    @Override
    public void setComputerInPosto(ComputerPostoRequestDto computerInPosto) {
        computerPostoEntity = new ComputerPostoEntity(computerInPosto);
        computerPostoRepoFactory.save(computerPostoEntity);
    }

    // NOTE: O que não for preenchido, dentro do banco de dados será NULL
    @Override
    public void putComputerInPosto(UUID uuid, ComputerPostoRequestDto computerPutInPosto) {
        computerPostoEntity = computerPostoRepoFactory.findById(uuid).orElseThrow(() -> new ExNotFound("Computador com uuid não encontrado: " + uuid));

        // NOTE: (novoValor, valorAtual, valorIgnorado)
        BeanUtils.copyProperties(computerPutInPosto, computerPostoEntity, "idPc");
        computerPostoRepoFactory.save(computerPostoEntity);
    }

    @Override
    public void patchComputerInPosto(UUID uuid, ComputerPostoRequestDto computerPatchInPosto) {
        computerPostoEntity = computerPostoRepoFactory.findById(uuid).orElseThrow(() -> new ExNotFound("Computador com uuid não encontrado: " + uuid));

        String[] getNullPropertyNames = GetNullPropertyNames.nullPropertyNames(computerPatchInPosto);

        // NOTE: (novoValor, valorAtual, valorIgnorado)
        BeanUtils.copyProperties(computerPatchInPosto, computerPostoEntity, getNullPropertyNames);
        computerPostoRepoFactory.save(computerPostoEntity);
    }

    @Override
    public void deleteComputerInPosto(UUID uuid) {
        computerPostoEntity = computerPostoRepoFactory.findById(uuid).orElseThrow(() -> new ExNotFound("Computador com uuid não encontrado: " + uuid));
        computerPostoRepoFactory.delete(computerPostoEntity);
    }
}
