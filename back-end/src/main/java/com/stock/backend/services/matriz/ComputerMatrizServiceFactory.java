package com.stock.backend.services.matriz;

import com.stock.backend.dto.matriz.ComputerMatrizRequestDto;
import com.stock.backend.dto.matriz.ComputerMatrizResponseDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.UUID;

public interface ComputerMatrizServiceFactory {
    Page<ComputerMatrizResponseDto> getAllComputersInMatriz(Pageable pageable);
    ComputerMatrizResponseDto getComputerInMatrizByUUID(UUID uuid);
    void setComputerInMatriz(ComputerMatrizRequestDto computerInMatriz);
    void putComputerInMatriz(UUID uuid, ComputerMatrizRequestDto computerPutInMatriz);

    void patchComputerInMatriz(UUID uuid, ComputerMatrizRequestDto computerPatchInMatriz);
    void deleteComputerInMatriz(UUID uuid);
}
