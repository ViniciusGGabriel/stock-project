package com.stock.backend.controllers;

import com.stock.backend.dto.matriz.ComputerMatrizRequestDto;
import com.stock.backend.dto.matriz.ComputerMatrizResponseDto;
import com.stock.backend.exceptions.exceptionsHandler.restHandler.RestOperationStatus;
import com.stock.backend.services.matriz.ComputerMatrizService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping(value = "/v2/matriz")
public class ComputerMatrizController {
    @Autowired
    private ComputerMatrizService computerMatrizService;

    @GetMapping
    @CrossOrigin(origins = "*", allowedHeaders = "*") // TODO: origins="ip frontend"
    @RequestMapping(value = "/computers", method = RequestMethod.GET)
    public Page<ComputerMatrizResponseDto> getAllComputers(Pageable pageable) {
        return computerMatrizService.getAllComputersInMatriz(pageable);
    }

    @GetMapping
    @CrossOrigin(origins = "*", allowedHeaders = "*") // TODO: origins="ip frontend"
    @RequestMapping(value = "/computers/{uuid-matriz}", method = RequestMethod.GET)
    public ResponseEntity<ComputerMatrizResponseDto> getComputerByUUID(@PathVariable("uuid-matriz") UUID uuid) {
        return ResponseEntity.status(HttpStatus.OK).body(computerMatrizService.getComputerInMatrizByUUID(uuid));
    }

    @PostMapping
    @CrossOrigin(origins = "*", allowedHeaders = "*") // TODO: origins="ip frontend"
    @RequestMapping(value = "/computers", method = RequestMethod.POST)
    public ResponseEntity<RestOperationStatus> postComputer(@RequestBody ComputerMatrizRequestDto computerPostInMatriz) {
        computerMatrizService.setComputerInMatriz(computerPostInMatriz);

        RestOperationStatus restOperationStatus = new RestOperationStatus(HttpStatus.CREATED, "Dados salvos com sucesso!");
        return ResponseEntity.status(HttpStatus.CREATED).body(restOperationStatus);
    }
    // TODO: put route
    @PutMapping
    @CrossOrigin(origins = "*", allowedHeaders = "*") // TODO: origins="ip frontend"
    @RequestMapping(value = "/computers/update/{uuid-matriz}", method = RequestMethod.PUT)
    public ResponseEntity<RestOperationStatus> putComputer(@PathVariable("uuid-matriz") UUID uuid, @RequestBody ComputerMatrizRequestDto computerPutInMatriz) {
        computerMatrizService.putComputerInMatriz(uuid, computerPutInMatriz);

        RestOperationStatus restOperationStatus = new RestOperationStatus(HttpStatus.OK, "Dados alterados com sucesso!");
        return ResponseEntity.status(HttpStatus.OK).body(restOperationStatus);
    }

    @PatchMapping
    @CrossOrigin(origins = "*", allowedHeaders = "*") // TODO: origins="ip frontend"
    @RequestMapping(value = "/computers/patch/{uuid-matriz}", method = RequestMethod.PATCH)
    public ResponseEntity<RestOperationStatus> patchComputer(@PathVariable("uuid-matriz") UUID uuid, @RequestBody ComputerMatrizRequestDto computerPatchInMatriz) {
        computerMatrizService.patchComputerInMatriz(uuid, computerPatchInMatriz);

        RestOperationStatus restOperationStatus = new RestOperationStatus(HttpStatus.OK, "Dados alterados com sucesso!");
        return ResponseEntity.status(HttpStatus.OK).body(restOperationStatus);
    }

    @DeleteMapping
    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @RequestMapping(value = "/computers/delete/{uui-matriz}", method = RequestMethod.DELETE)
    public ResponseEntity<RestOperationStatus> deleteComputer(@PathVariable("uui-matriz") UUID uuid) {
        computerMatrizService.deleteComputerInMatriz(uuid);

        RestOperationStatus restOperationStatus = new RestOperationStatus(HttpStatus.OK, "Valor deletado com sucesso!");
        return ResponseEntity.status(HttpStatus.OK).body(restOperationStatus);
    }
}