package com.stock.backend.controllers;

import com.stock.backend.dto.estoque.ComputerEstoqueRequestDto;
import com.stock.backend.dto.estoque.ComputerEstoqueResponseDto;
import com.stock.backend.exceptions.exceptionsHandler.restHandler.RestOperationStatus;
import com.stock.backend.services.estoque.ComputerEstoqueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping(value = "/v2/estoque")
public class ComputerEstoqueController {
    @Autowired
    private ComputerEstoqueService computerEstoqueService;

    @GetMapping
    @CrossOrigin(origins = "*", allowedHeaders = "*") // TODO: origins="ip frontend"
    @RequestMapping(value = "/computers", method = RequestMethod.GET)
    public Page<ComputerEstoqueResponseDto> getAllComputers(Pageable pageable) {
        return computerEstoqueService.getAllComputersInEstoque(pageable);
    }

    @GetMapping
    @CrossOrigin(origins = "*", allowedHeaders = "*") // TODO: origins="ip frontend"
    @RequestMapping(value = "/computers/{uuid-estoque}", method = RequestMethod.GET)
    public ResponseEntity<ComputerEstoqueResponseDto> getComputerByUUID(@PathVariable("uuid-estoque") UUID uuid) {
        return ResponseEntity.status(HttpStatus.OK).body(computerEstoqueService.getComputerInEstoqueByUUID(uuid));
    }

    @PostMapping
    @CrossOrigin(origins = "*", allowedHeaders = "*") // TODO: origins="ip frontend"
    @RequestMapping(value = "/computers", method = RequestMethod.POST)
    public ResponseEntity<RestOperationStatus> postComputer(@RequestBody ComputerEstoqueRequestDto computerPostInEstoque) {
        computerEstoqueService.setComputerInEstoque(computerPostInEstoque);

        RestOperationStatus restOperationStatus = new RestOperationStatus(HttpStatus.CREATED, "Dados salvos com sucesso!");
        return ResponseEntity.status(HttpStatus.CREATED).body(restOperationStatus);
    }

    @PutMapping
    @CrossOrigin(origins = "*", allowedHeaders = "*") // TODO: origins="ip frontend"
    @RequestMapping(value = "/computers/update/{uuid-estoque}", method = RequestMethod.PUT)
    public ResponseEntity<RestOperationStatus> putComputer(@PathVariable("uuid-estoque") UUID uuid, @RequestBody ComputerEstoqueRequestDto computerPutInEstoque) {
        computerEstoqueService.putComputerInEstoque(uuid, computerPutInEstoque);

        RestOperationStatus restOperationStatus = new RestOperationStatus(HttpStatus.OK, "Dados alterados com sucesso!");
        return ResponseEntity.status(HttpStatus.OK).body(restOperationStatus);
    }

    @PatchMapping
    @CrossOrigin(origins = "*", allowedHeaders = "*") // TODO: origins="ip frontend"
    @RequestMapping(value = "/computers/patch/{uuid-estoque}", method = RequestMethod.PATCH)
    public ResponseEntity<RestOperationStatus> patchComputer(@PathVariable("uuid-estoque") UUID uuid, @RequestBody ComputerEstoqueRequestDto computerPatchInEstoque) {
        computerEstoqueService.patchComputerInEstoque(uuid, computerPatchInEstoque);

        RestOperationStatus restOperationStatus = new RestOperationStatus(HttpStatus.OK, "Dados alterados com sucesso!");
        return ResponseEntity.status(HttpStatus.OK).body(restOperationStatus);
    }

    @DeleteMapping
    @CrossOrigin(origins = "*", allowedHeaders = "*") // TODO: origins="ip frontend"
    @RequestMapping(value = "/computers/delete/{uui-estoque}", method = RequestMethod.DELETE)
    public ResponseEntity<RestOperationStatus> deleteComputer(@PathVariable("uui-estoque") UUID uuid) {
        computerEstoqueService.deleteComputerInEstoque(uuid);

        RestOperationStatus restOperationStatus = new RestOperationStatus(HttpStatus.OK, "Valor deletado com sucesso!");
        return ResponseEntity.status(HttpStatus.OK).body(restOperationStatus);
    }
}
