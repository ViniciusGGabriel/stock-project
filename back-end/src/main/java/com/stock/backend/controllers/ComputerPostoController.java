package com.stock.backend.controllers;

import com.stock.backend.dto.posto.ComputerPostoRequestDto;
import com.stock.backend.dto.posto.ComputerPostoResponseDto;
import com.stock.backend.exceptions.exceptionsHandler.restHandler.RestOperationStatus;
import com.stock.backend.services.posto.ComputerPostoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping(value = "/v2/posto")
public class ComputerPostoController {
    @Autowired
    private ComputerPostoService computerPostoService;

    @GetMapping
    @CrossOrigin(origins = "*", allowedHeaders = "*") // TODO: origins="ip frontend"
    @RequestMapping(value = "/computers", method = RequestMethod.GET)
    public Page<ComputerPostoResponseDto> getAllComputers(Pageable pageable) {
        return computerPostoService.getAllComputersInPosto(pageable);
    }

    @GetMapping
    @CrossOrigin(origins = "*", allowedHeaders = "*") // TODO: origins="ip frontend"
    @RequestMapping(value = "/computers/{uuid-posto}", method = RequestMethod.GET)
    public ResponseEntity<ComputerPostoResponseDto> getComputerByUUID(@PathVariable("uuid-posto") UUID uuid) {
        return ResponseEntity.status(HttpStatus.OK).body(computerPostoService.getComputerInPostoByUUID(uuid));
    }

    @PostMapping
    @CrossOrigin(origins = "*", allowedHeaders = "*") // TODO: origins="ip frontend"
    @RequestMapping(value = "/computers", method = RequestMethod.POST)
    public ResponseEntity<RestOperationStatus> postComputer(@RequestBody ComputerPostoRequestDto computerPostInPosto) {
        computerPostoService.setComputerInPosto(computerPostInPosto);

        RestOperationStatus restOperationStatus = new RestOperationStatus(HttpStatus.CREATED, "Dados salvos com sucesso!");
        return ResponseEntity.status(HttpStatus.CREATED).body(restOperationStatus);
    }

    @PutMapping
    @CrossOrigin(origins = "*", allowedHeaders = "*") // TODO: origins="ip frontend"
    @RequestMapping(value = "/computers/update/{uuid-posto}", method = RequestMethod.PUT)
    public ResponseEntity<RestOperationStatus> putComputer(@PathVariable("uuid-posto") UUID uuid, @RequestBody ComputerPostoRequestDto computerPutInPosto) {
        computerPostoService.putComputerInPosto(uuid, computerPutInPosto);

        RestOperationStatus restOperationStatus = new RestOperationStatus(HttpStatus.OK, "Dados alterados com sucesso!");
        return ResponseEntity.status(HttpStatus.OK).body(restOperationStatus);
    }

    @PatchMapping
    @CrossOrigin(origins = "*", allowedHeaders = "*") // TODO: origins="ip frontend"
    @RequestMapping(value = "/computers/patch/{uuid-posto}", method = RequestMethod.PATCH)
    public ResponseEntity<RestOperationStatus> patchComputer(@PathVariable("uuid-posto") UUID uuid, @RequestBody ComputerPostoRequestDto computerPatchInPosto) {
        computerPostoService.patchComputerInPosto(uuid, computerPatchInPosto);

        RestOperationStatus restOperationStatus = new RestOperationStatus(HttpStatus.OK, "Dados alterados com sucesso!");
        return ResponseEntity.status(HttpStatus.OK).body(restOperationStatus);
    }

    @DeleteMapping
    @CrossOrigin(origins = "*", allowedHeaders = "*") // TODO: origins="ip frontend"
    @RequestMapping(value = "/computers/delete/{uui-posto}", method = RequestMethod.DELETE)
    public ResponseEntity<RestOperationStatus> deleteComputer(@PathVariable("uui-posto") UUID uuid) {
        computerPostoService.deleteComputerInPosto(uuid);

        RestOperationStatus restOperationStatus = new RestOperationStatus(HttpStatus.OK, "Valor deletado com sucesso!");
        return ResponseEntity.status(HttpStatus.OK).body(restOperationStatus);
    }
}
