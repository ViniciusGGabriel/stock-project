CREATE TABLE computadores_matriz (
    ID_PC uuid PRIMARY KEY DEFAULT gen_random_uuid(), -- ID computador gerado pelo BD em UUID
    DEPARTAMENTO VARCHAR(100) NOT NULL, -- Departamento em que o pc está
    UNIDADE_DE_NEGOCIO CHAR(16) CHECK(UNIDADE_DE_NEGOCIO in ('HOSPITAL', 'LABORATORIO','BANCO_DE_SANGUE','CEMEG')) NOT NULL, -- Unidade de negócio em que está o pc
    NUMERO_PATRIMONIO CHAR(6), -- Numero do patrimonio completo 000000
    TIPO_COMPUTADOR VARCHAR(15) CHECK (TIPO_COMPUTADOR IN ('DESKTOP', 'MONTADO', 'ALL IN ONE', 'MICRO', 'NOTEBOOK')) NOT NULL, -- Modelo do computador
    NOME_COMPUTADOR VARCHAR(15) NOT NULL UNIQUE, -- Nome único do computador dentro do AD
    IPV4 CHAR(15) NOT NULL UNIQUE, -- IP dentro do DHCP do computador
    CPU VARCHAR(15), -- Processador do pc
    MEMORIA_RAM_GB INTEGER, -- Tamanho da memoria ram 8Gb, 16GB
    FREQUENCIA_RAM INTEGER, -- Frequência em que a memoria funciona
    TIPO_RAM VARCHAR(10) CHECK (TIPO_RAM IN ('DIMM', 'SODIMM', 'SOLDADA')), -- Arquitetura da memoria ram
    MODELO_RAM VARCHAR(5) CHECK(MODELO_RAM IN ('DDR4', 'DDR3')), -- Compatibilidade da ram
    QUANT_RAM_INSTALADA INTEGER, -- Quantidade de ram instalada
    HD_GB INTEGER, -- Em gigabytes
    SSD_GB INTEGER, -- Em gigabytes
    TASY_AGENT BOOLEAN, -- TASY agent instalado
    LOCAL_PC VARCHAR(15) CHECK (LOCAL_PC IN ('MATRIZ', 'POSTO', 'ESTOQUE')) NOT NULL, -- Local em que o pc está
    MARCA_PC VARCHAR(15), -- Marca do pc Dell, Lenovo ou montado
    SO CHAR(3) CHECK (SO IN ('W10', 'W11', 'W7')) -- Sistema operacional
);

COMMENT ON COLUMN computadores_matriz.ID_PC IS 'ID computador gerado pelo BD em UUID';
COMMENT ON COLUMN computadores_matriz.DEPARTAMENTO IS 'Departamento em que o PC está';
COMMENT ON COLUMN computadores_matriz.UNIDADE_DE_NEGOCIO IS 'Unidade de negócio do computador';
COMMENT ON COLUMN computadores_matriz.NUMERO_PATRIMONIO IS 'Numero do patrimonio completo 000000';
COMMENT ON COLUMN computadores_matriz.TIPO_COMPUTADOR IS 'Modelo do computador';
COMMENT ON COLUMN computadores_matriz.NOME_COMPUTADOR IS 'Nome único do computador dentro do AD';
COMMENT ON COLUMN computadores_matriz.IPV4 IS 'IP dentro do DHCP do computador';
COMMENT ON COLUMN computadores_matriz.CPU IS 'Processador do pc';
COMMENT ON COLUMN computadores_matriz.MEMORIA_RAM_GB IS 'Tamanho da memoria ram 8Gb, 16GB';
COMMENT ON COLUMN computadores_matriz.FREQUENCIA_RAM IS 'Frequência em que a memoria funciona';
COMMENT ON COLUMN computadores_matriz.TIPO_RAM IS 'Arquitetura da memoria ram DIMM,SODIMM';
COMMENT ON COLUMN computadores_matriz.MODELO_RAM IS 'Compatibilidade da ram DDR3, DDR4';
COMMENT ON COLUMN computadores_matriz.QUANT_RAM_INSTALADA IS 'Quantidade de ram instalada';
COMMENT ON COLUMN computadores_matriz.HD_GB IS 'Hd em gigabyte';
COMMENT ON COLUMN computadores_matriz.SSD_GB IS 'Sdd em gigabyte';
COMMENT ON COLUMN computadores_matriz.TASY_AGENT IS 'TASY agent instalado';
COMMENT ON COLUMN computadores_matriz.LOCAL_PC IS 'Local em que o pc está';
COMMENT ON COLUMN computadores_matriz.MARCA_PC IS 'Marca do pc Dell, Lenovo ou montado';
COMMENT ON COlUMN computadores_matriz.SO IS 'Sistema operacional';