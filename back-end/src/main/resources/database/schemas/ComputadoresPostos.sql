CREATE TABLE computadores_postos (
    ID_PC uuid PRIMARY KEY DEFAULT gen_random_uuid(), -- ID computador gerado pelo BD em UUID
    DEPARTAMENTO VARCHAR(16) CHECK(DEPARTAMENTO IN ('AREA_TECNICA', 'PRONTO_SOCORRO', 'RECEPCAO', 'COLETA')) NOT NULL, -- Departamento em que o pc está
    LOCAL_POSTO VARCHAR(22) CHECK(LOCAL_POSTO IN ('IRG', 'SANTA_BARBARA','HOSPITAL_CORACAO','SAO_FRANCISCO', 'ORION', 'HIC')) NOT NULL, -- Posto em que o computador está
    NUMERO_PATRIMONIO CHAR(6), -- Numero do patrimonio completo 000000
    TIPO_COMPUTADOR VARCHAR(15) CHECK (TIPO_COMPUTADOR IN ('DESKTOP', 'MONTADO', 'ALL IN ONE', 'MICRO', 'NOTEBOOK')), -- Modelo do computador
    NOME_COMPUTADOR VARCHAR(15) NOT NULL UNIQUE, -- Nome único do computador dentro do AD
    IPV4 CHAR(15) NOT NULL UNIQUE, -- IP dentro do DHCP do computador
    CPU VARCHAR(15), -- Processador do pc
    MEMORIA_RAM_GB INTEGER, -- Tamanho da memoria ram 8Gb, 16GB
    FREQUENCIA_RAM INTEGER, -- Frequência em que a memoria funciona
    TIPO_RAM VARCHAR(10) CHECK (TIPO_RAM IN ('DIMM', 'SODIMM', 'SOLDADA')), -- Arquitetura da memoria ram
    MODELO_RAM VARCHAR(5) CHECK(MODELO_RAM IN ('DDR4', 'DDR3')), -- Compatibilidade da ram
    QUANT_RAM_INSTALADA INTEGER, -- Quantidade de ram instalada
    HD_GB INTEGER, -- Em gigabytes
    SSD_GB INTEGER, -- Em gigabytes
    TASY_AGENT BOOLEAN, -- TASY agent instalado
    LOCAL_PC VARCHAR(15) CHECK (LOCAL_PC IN ('MATRIZ', 'POSTO', 'ESTOQUE')) NOT NULL, -- Local em que o pc está
    MARCA_PC VARCHAR(15), -- Marca do pc Dell, Lenovo ou montado
    SO CHAR(3) CHECK (SO IN ('W10', 'W11', 'W7')) -- Sistema operacional
);

COMMENT ON COLUMN computadores_postos.ID_PC IS 'ID computador gerado pelo BD em UUID';
COMMENT ON COLUMN computadores_postos.DEPARTAMENTO IS 'Departamento em que o PC está';
COMMENT ON COLUMN computadores_postos.LOCAL_POSTO IS 'Posto em que o computador está';
COMMENT ON COLUMN computadores_postos.NUMERO_PATRIMONIO IS 'Numero do patrimonio completo 000000';
COMMENT ON COLUMN computadores_postos.TIPO_COMPUTADOR IS 'Modelo do computador';
COMMENT ON COLUMN computadores_postos.NOME_COMPUTADOR IS 'Nome único do computador dentro do AD';
COMMENT ON COLUMN computadores_postos.IPV4 IS 'IP dentro do DHCP do computador';
COMMENT ON COLUMN computadores_postos.CPU IS 'Processador do pc';
COMMENT ON COLUMN computadores_postos.MEMORIA_RAM_GB IS 'Tamanho da memoria ram 8Gb, 16GB';
COMMENT ON COLUMN computadores_postos.FREQUENCIA_RAM IS 'Frequência em que a memoria funciona';
COMMENT ON COLUMN computadores_postos.TIPO_RAM IS 'Arquitetura da memoria ram DIMM,SODIMM';
COMMENT ON COLUMN computadores_postos.MODELO_RAM IS 'Compatibilidade da ram DDR3, DDR4';
COMMENT ON COLUMN computadores_postos.QUANT_RAM_INSTALADA IS 'Quantidade de ram instalada';
COMMENT ON COLUMN computadores_postos.HD_GB IS 'Hd em gigabyte';
COMMENT ON COLUMN computadores_postos.SSD_GB IS 'Sdd em gigabyte';
COMMENT ON COLUMN computadores_postos.TASY_AGENT IS 'TASY agent instalado';
COMMENT ON COLUMN computadores_postos.LOCAL_PC IS 'Local em que o pc está';
COMMENT ON COLUMN computadores_postos.MARCA_PC IS 'Marca do pc Dell, Lenovo ou montado';
COMMENT ON COlUMN computadores_postos.SO IS 'Sistema operacional';