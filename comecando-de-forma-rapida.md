---
description: Aqui você vera como subir a API no seu ambiente e já sair fazendo testes
---

# Começando de forma rápida

## Instale o docker em seu ambiente

O docker é necessário pois toda a api foi feita pensando em já ser contida em um container

## Instalando as dependências

Para a instalação das dependencias é necessario o maven 3.6.0 ou versão superior


#### Maven
```
# Install via MVN
mvn clean package
```

## Faça seu primeiro request

Execute o arquivo  back-end/src/main/java/com/stock/backend/BackEndApplication.java e após isso acesse [https://localhost:8080/swagger-ui.html](http://0.0.0.0:8080/swagger-ui/index.html)

## Criando seu primeiro post

<mark style="color:green;">`POST`</mark> `https://localhost:8080/v2/matriz/computers`

Após realizar uma requisição pelo swagger utilizando essa URL, para ver outras rotas da api é só trocar o matriz por posto ou estoque

    201 Computador criado
```Json
{
  "dadosGerais": {
    "departamento": "AREA_TECNICA",
    "localPosto": "IRG",
    "numeroPatrimonio": "string",
    "localPc": "MATRIZ",
    "marcaPc": "string"
  },
  "hardware": {
    "tipoComputador": "DESKTOP",
    "nomeComputador": "string",
    "ip": "string",
    "cpu": "string",
    "memoriaRamGb": 0,
    "frequenciaRam": 0,
    "tipoRam": "DIMM",
    "modeloRam": "DDR4",
    "quantRamInstalada": 0,
    "hdGb": 0,
    "ssdGb": 0
  },
  "software": {
    "tasyAgent": true,
    "so": "W11"
  }
}
```
    404 Computador inserido errado
```json
{
  "httpStatus": "BAD_REQUEST",
  "httpMessage": "Dados inseridos estão errados!"
}
```

    404 Conflito
```json
{
  "httpStatus": "BAD_REQUEST",
  "httpMessage": "O computador inserido já existe!"
}
```

Inserir uma requisição via curl:


    curl
```
curl -X 'POST' \
  'http://0.0.0.0:8080/v2/posto/computers' \
  -H 'accept: */*' \
  -H 'Content-Type: application/json' \
  -d '{
  "dadosGerais": {
    "departamento": "AREA_TECNICA",
    "localPosto": "IRG",
    "numeroPatrimonio": "string",
    "localPc": "MATRIZ",
    "marcaPc": "string"
  },
  "hardware": {
    "tipoComputador": "DESKTOP",
    "nomeComputador": "string",
    "ip": "string",
    "cpu": "string",
    "memoriaRamGb": 0,
    "frequenciaRam": 0,
    "tipoRam": "DIMM",
    "modeloRam": "DDR4",
    "quantRamInstalada": 0,
    "hdGb": 0,
    "ssdGb": 0
  },
  "software": {
    "tasyAgent": true,
    "so": "W11"
  }
}'
```
